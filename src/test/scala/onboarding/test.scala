package onboarding

import common.TokenGenerate
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._

import scala.config.Config

class test extends Simulation {

  val tokenGenerate: TokenGenerate = new TokenGenerate
  val httpProtocol = {

    val token: String = tokenGenerate.getToken()
    print(token)

    http
      .baseUrl(Config.base_url) // Here is the root for all relative URLs
      .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
      .acceptEncodingHeader("gzip, deflate")
      .acceptLanguageHeader("en-US,en;q=0.5")
      .authorizationHeader(s"Bearer $token")
      .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")
  }

  /*
 * A HTTP protocol builder is used to specify common properties of request(s) to be sent,
 * for instance the base URL, HTTP headers that are to be enclosed with all requests etc.
 */
  val theScenarioBuilder: ScenarioBuilder = scenario("Scenario1")
    .exec(
      http("myRequest1")
        .get("/computers"))


  setUp(theScenarioBuilder.inject(atOnceUsers(10)).protocols(httpProtocol))


  /* Place for arbitrary Scala code that is to be executed before the simulation begins. */
  before {
    println("***** My simulation is about to begin! *****")
  }

  /* Place for arbitrary Scala code that is to be executed after the simulation has ended. */
  after {
    println("***** My simulation has ended! ******")
  }


}

